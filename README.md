# IIAS

1. Obtener shellcode.o a partir de shellcode.asm utilizando NASM: 

``
.\nasm.exe -f win32 -o shellcode.o shellcode_corta.asm
``

2. Obtener el código máquina a partir de la salida de NASM. Se puede usar la siguiente web: https://www.jdoodle.com/test-bash-shell-script-online/
> **Nota:** Hay que subir el archivo shellcode.o con el botón que está al lado del "Execute"

``
for i in $(objdump -d /uploads/shellcode.o -M intel |grep "^ " |cut -f2); do echo -n '\x'$i; done;echo
``

> **Nota:** Si no se usa la web, ajustar la ruta del fichero. En el caso de la web, dicha ruta es /uploads/shellcode.o
